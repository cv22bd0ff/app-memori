<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\
{
    HomeController, CriarController
};


//Autenticação
Route::get('/', [HomeController::class,'index']);
Route::get('/atualizar', [HomeController::class, 'atualizar']);
Route::get('/dados', [HomeController::class, 'dados']);
Route::get('/termina', [HomeController::class, 'termina']);

//Criação
Route::get('/criar/create', [CriarController::class, 'criacao']);
Route::post('/lista', [CriarController::class, 'lista']);
