<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CriarController extends Controller
{
   //Criação
   public function criacao(Request $request)
   {  
       //Dados
       $cep= $request->input('cep');
       $cidade = $request->input('cidade');
       $vizinhaca = $request->input('vizinhaca');
       $numero = $request->input('numero');
       $estado = $request->input('estado');
       $rua = $request->input('rua');
       $complemento = $request->input('complemento');
       $aniversario = $request->input('aniversario');
       $celular = $request->input('celular');
       $cpf = $request->input('cpf');
       $email = $request->input('email');
       $genero = $request->input('genero');
       $nome = $request->input('nome');
       $senha = $request->input('senha');
       $ComfirmaSenha = $request->input('ComfirmaSenha');
       
       $morado = Http::post('https://virtserver.swaggerhub.com/Nucleus1/Memori/1.0.1/auth/client/create', [
           'cep'=> $cep,
           'city' => $cidade,
           'neighborhood' => $vizinhaca,
           'number' => $numero,
           'state' => $estado,
           'street' => $rua,
           'complement' => $complemento,
           'birthdate' => $aniversario,
           'cellular' => $celular,
           'cpf' => $cpf,
           'email' => $email,
           'gender' => $genero,
           'name' => $nome,
           'password' => $senha,
           'password_confirmation' => $ComfirmaSenha,
       ]);
       
       /* $morado->json();
       dd($morado->json()); */

        $morado = [
           'cep'=>$cep,
           'cidade' => $cidade,
           'vizinhaca' => $vizinhaca,
           'numero' => $numero,
           'estado' => $estado,
           'rua' => $rua,
           'complemento' => $complemento,
           'aniversario' => $aniversario,
           'celular' => $celular,
           'cpf' => $cpf,
           'email' => $email,
           'genero' => $genero,
           'nome' => $nome,
           'senha' => $senha,
           'ComfirmaSenha' => $ComfirmaSenha,
       ];
 
       return view('criar.create', $morado);
   }

   //lista de Clientes
   public function lista()
   {  
    
    //Lista
    $lista = Http::get('https://virtserver.swaggerhub.com/Nucleus1/Memori/1.0.1/auth/client/listAll');
    
    /* $lista->json();
    dd($lista->json());
    */
    $morador = $lista->json();

    return view('criar.lista', compact('morador'));
   }
}
