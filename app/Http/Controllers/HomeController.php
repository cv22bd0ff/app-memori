<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
   //index
   public function index(Request $request)
   {
        $email = $request->input('email');
        $senha = $request->input('senha');
        
        $response = Http::post('https://virtserver.swaggerhub.com/Nucleus1/Memori/1.0.1/auth/login', [
            'email' => $email,
            'password' => $senha,
        ]);

        /* $response->json();
        
       dd($response->json()); */

       $dados = [
           'email'=>$email,
           'senha'=>$senha,
           'mensagem'=>$response->json()['message']
       ];

       return view('/autenticacao.home', $dados);
   }

   //atualizar
   public function atualizar(Request $request)
    {  
        $id = $request->input('id');
        $nome = $request->input('nome');
        $apelido = $request->input('apelido');
        $data = $request->input('data');
        $cnpj = $request->input('cnpj');
        $categoria = $request->input('categoria');
        $documento = $request->input('documento');
        $responsavel = $request->input('responsavel');
        $telefone = $request->input('telefone');
        $ocupacao= $request->input('ocupacao');
        //Contato
        $email= $request->input('email');
        $fone= $request->input('fone');
        $site= $request->input('site');
        $instagram= $request->input('instagram');
        $facebook= $request->input('facebook');
        //Endereço
        $postal= $request->input('postal');
        $area= $request->input('area');
        $numero= $request->input('numero');
        $cidade= $request->input('cidade');
        $vizinhaca= $request->input('vizinhaca');
        $estado= $request->input('estado');
        $complemento= $request->input('complemento');
        //Imagens
        $logo= $request->input('logo');
        $banner= $request->input('banner');
        $img1= $request->input('img1');
        $img2= $request->input('img2');
        $img3= $request->input('img3');
        $img4= $request->input('img4');
        $img5= $request->input('img5');
        $img6= $request->input('img6');
        $img7= $request->input('img7');
        $img8= $request->input('img8');
        $img9= $request->input('img9');
        
        $atualizar = Http::get('https://virtserver.swaggerhub.com/Nucleus1/Memori/1.0.1/auth/refresh', [
            'id'=> $id,
            'name' => $nome,
            'fantasy_name' => $apelido,
            'services' => $data,
            'account_type' => $cnpj,
            'category_id' => $categoria,
            'doc_number' => $documento,
            'responsible_name' => $responsavel,
            'responsible_phone' => $telefone,
            'occupation' => $ocupacao,
            //Contato
            'email' => $email,
            'phone' => $fone,
            'website' => $site,
            'instagram' => $instagram,
            'facebook' => $facebook,
            //Endereço
            'zipcode' => $postal,
            'area' => $area,
            'number' => $numero,
            'city' => $cidade,
            'neighborhood' => $vizinhaca,
            'state' => $estado,
            'complement' => $complemento,
            //Imagens
            'logo' => $logo,
            'banner' => $banner,
            'one' => $img1,
            'two' => $img2,
            'three' => $img3,
            'four' => $img4,
            'five' => $img5,
            'six' => $img6,
            'seven' => $img7,
            'eight' => $img8,
            'nine' => $img9,
        ]);
    
        /* $atualizar->json();     
       dd($atualizar->json()); */

        $atualizar = [
            'id'=>$id,
            'nome' => $nome,
            'apelido' => $apelido,
            'data' => $data,
            'cnpj' => $cnpj,
            'categoria' => $categoria,
            'documento' => $documento,
            'responsavel' => $responsavel,
            'telefone' => $telefone,
            'ocupacao' => $ocupacao,
            //Contato
            'email' => $email,
            'fone' => $fone,
            'site' => $site,
            'instagram' => $instagram,
            'facebook' => $facebook,
            //Endereço
            'postal' => $postal,
            'area' => $area,
            'numero' => $numero,
            'cidade' => $cidade,
            'vizinhaca' => $vizinhaca,
            'estado' => $estado,
            'complemento' => $complemento,
            //Imagens
            'logo' => $logo,
            'banner' => $banner,
            'img1' => $img1,
            'img2' => $img2,
            'img3' => $img3,
            'img4' => $img4,
            'img5' => $img5,
            'img6' => $img6,
            'img7' => $img7,
            'img8' => $img8,
            'img9' => $img9,

            'mensagem'=>$atualizar->json()['message']
        ];

        return view('/autenticacao.atualizar', $atualizar);
    }

    //dados
    public function dados()
    {
        $dados = Http::get('https://virtserver.swaggerhub.com/Nucleus1/Memori/1.0.1/auth/account');
        $dados->json();
        dd($dados->json());

        return view('/autenticacao.dados', $dados);
    }

    //terminar
    public function termina()
    {
        $termina = Http::get('https://virtserver.swaggerhub.com/Nucleus1/Memori/1.0.1/auth/logout');
        $termina->json(); 
        dd($termina->json());

        return view('/autenticacao.termina', $termina);
    }
}
