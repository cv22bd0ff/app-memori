@extends('layouts.main')

@section('title', 'App Memori')

@section('content')

<div id="event-create-container" class="col-md-6 offset-md-3">
    
    <form action="/lista" method="POST" enctype="multipart/form-data">
    @csrf

        <h1>Dados</h1>
        <div class="form-group">
            <label for="title">Nome</label>
            <input type="text" class="form-control" id="name" name="name" placeholder="Seu nome">
        </div>
        <div class="form-group">
            <label for="title">Telefone</label>
            <input type="text" class="form-control" id="phone" name="numero" placeholder="Seu número">
        </div>
        <div class="form-group">
            <label for="title">Data de Nascimento</label>
            <input type="text" class="form-control" id="birthdate" name="aniversario" placeholder="dd/mm/aaaa">
        </div>
        <div class="form-group">
            <label for="title">CPF</label>
            <input type="texte" class="form-control" id="cpf" name="cpf" placeholder="000.000.000-00">
        </div>
        <div class="form-group">
            <label for="title">Sexo</label>
            <select name="genero" id="gender" class="form-control">
                <option value="0">Masculino</option>
                <option value="1">Feminino</option>
            </select>
        </div>

        <h1>Endereço</h1>
        <div class="form-group">
            <label for="title">Rua</label>
            <input type="text" class="form-control" id="street" name="street" placeholder="Sua rua">
        </div>
        <div class="form-group">
            <label for="title">Número</label>
            <input type="text" class="form-control" id="number" name="number" placeholder="Número">
        </div>
        <div class="form-group">
            <label for="title">complemento</label>
            <input type="text" class="form-control" id="complement" name="complement" placeholder="Casa, Apto, Loja">
        </div>
        <div class="form-group">
            <label for="title">Cep</label>
            <input type="number" class="form-control" id="cep" name="cep" placeholder="Cep">
        </div>
        <div class="form-group">
            <label for="title">Cidade</label>
            <input type="text" class="form-control" id="city" name="city" placeholder="Fortaleza">
        </div>
        <div class="form-group">
            <label for="title">Estado</label>
            <input type="text" class="form-control" id="state" name="state" placeholder="CE">
        </div>
        <div class="form-group">
            <label for="title">Bairro</label>
            <input type="text" class="form-control" id="neighborhood" name="neighborhood" placeholder="Centro">
        </div>
        
        <input type="submit" class="btn btn-primary" value="Enviar">

    </form> 
</div>

@endsection
