@extends('layouts.main')

@section('title', 'Inicio')

@section('content')

<div id="event-create-container" class="col-md-6 offset-md-3">
    <h1>App Memori</h1>
    <form action=" " method="POST" enctype="multipart/form-data">
    @csrf

        <div class="form-group">
            <label for="title">Usuario:</label>
            <input type="text" class="form-control" id="email" name="email" placeholder="Seu E-mail">
        </div>
        <div class="form-group">
            <label for="title">Senha:</label>
            <input type="password" class="form-control" id="password" name="senha" placeholder="Sua Senha">
        </div>
        <input type="submit" class="btn btn-primary" value="Entrar">
        
    </form>
    <label><p>{{$mensagem}}</p></label>
</div>

@endsection
